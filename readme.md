# Gulp-Reload
*HTML/CSS/JS live reloading for gulp tasks*

Features:
* Refreshless CSS reloading
* Easy to install into your gulpfile
* Auto-injects reloading client into one of your JS files (only when activated)

A few important notes:
* Runs on port 8082 by default (for easy cloud9 support!)
* To set custom port pass port in as param. Eg. `var reload = new GulpReload(1234);`
* If using externally, make sure your firewall rules permit the port you've chosen :)

**Remember to run gulp with `gulp --live` to activate live reloading!**

## Usage example.
```
#!javascript
console.log('\033[2J');

var gulp = require('gulp');
var util = require('gulp-util');
var pug = require('gulp-pug');
var less = require('gulp-less');
var minifyCSS = require('gulp-csso');
var minify = require('gulp-minify');

if (util.env.live) {
  var GulpReload = require('gulp-reload');
  var reload = new GulpReload();
}

var path = {
  build: {
    html: 'dist',
    css: 'dist/style',
    js: 'dist/script',
  },
  src: {
    html: 'src/**/*.jade',
    css: 'src/style/*.less',
    js: 'src/script/**/*.js',
  },
  watch: {
    html: 'src/**/*.jade',
    css: 'src/style/**/*.less',
    js: 'src/script/**/*.js',
  }
};

var tasks = ['external', 'html', 'css', 'js'];

if (!util.env.production) tasks.push('watch');

gulp.task('default', tasks);

gulp.task('external', function() {
  return gulp.src(['external/**/*'])
    .pipe(gulp.dest('dist'));
});

gulp.task('html', function() {
  return gulp.src(path.src.html)
    .pipe(pug())
    .on('error', swallowError)
    .pipe(gulp.dest(path.build.html)).on('end', function() {
      if (util.env.live) reload.refresh()
    })
});

gulp.task('css', function() {
  return gulp.src(path.src.css)
    .pipe(less())
    .on('error', swallowError)
    .pipe(minifyCSS())
    .pipe(gulp.dest(path.build.css)).on('end', function() {
      if (util.env.live) reload.refreshStyles()
    })
});

gulp.task('js', function() {
  return gulp.src(path.src.js)
    .pipe(util.env.production ? minify({
      ext: {
        min: '.js'
      },
      noSource: true,
      ignoreFiles: ['.min.js']
    }) : util.noop())
    .on('error', swallowError)
    .pipe(util.env.live ? reload.injectClient() : util.noop())
    .pipe(gulp.dest(path.build.js)).on('end', function() {
      if (util.env.live) reload.refresh()
    })
});

gulp.task('watch', function() {
  gulp.watch(path.watch.html, {
    interval: 500
  }, ['html']);
  gulp.watch(path.watch.css, {
    interval: 500
  }, ['css']);
  gulp.watch(path.watch.js, {
    interval: 500
  }, ['js']);
  gulp.watch('external/**/*', {
    interval: 10000
  }, ['external']);
});

function swallowError(error) {
  // If you want details of the error in the console
  console.log(error.toString())
  this.emit('end')
}

```