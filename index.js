var fs = require('fs');
var through = require('through2');
var gutil = require('gulp-util');
var clientLocation = require.resolve('socket.io-client');
clientLocation = require('path').dirname(clientLocation);
var PluginError = gutil.PluginError;

var activated = !!gutil.env.live;

function prefixStream(prefixText) {
  var stream = through();
  stream.write(prefixText);
  return stream;
}

function c9Reload(port) {
  if (!activated) return;
  this.port = port || 8082;
  this.io = require('socket.io')(this.port);
  this.clientAppendFile = false;
  gutil.log('Cloud9 Reload', gutil.colors.magenta('listening on port ' + this.port + '.'));
}
 
c9Reload.prototype.refresh = function() {
  if (!activated) return gutil.noop();
  
  this.io.sockets.emit('refreshAll');
  return through.obj(function(file, encoding, callback) {
    callback(null, file);
  });
}

c9Reload.prototype.refreshStyles = function() {
  if (!activated) return gutil.noop();
  
  this.io.sockets.emit('refreshStyles');
  return through.obj(function(file, encoding, callback) {
    callback(null, file);
  });
}

c9Reload.prototype.injectClient = function() {
  var self = this;
  if (!activated) return through.obj(function(file, enc, cb) {
    this.push(file); cb();
  });
  
  var self = this;
  return through.obj(function(file, enc, cb) {
    if (file.isStream()) {
      this.emit('error', new PluginError("c9 Reload", 'Streams are not supported!'));
      return cb();
    }
    
    // Don't append the the client to multiple files
    if (self.clientAppendFile && self.clientAppendFile != file.path) {
      this.push(file);
      return cb();
    }
    if (!self.clientAppendFile) self.clientAppendFile = file.path;
    
    var io = new Buffer("(function(){" + fs.readFileSync(clientLocation + '/../dist/socket.io.slim.min.js', 'utf8'));
    var client = new Buffer("var port = " + self.port + ";\n" + fs.readFileSync(__dirname + '/client.js', 'utf8') + "})();\n");
    var clientPackage = new Buffer(Buffer.concat([io, client]));
    if (file.isBuffer()) {
      file.contents = Buffer.concat([file.contents, clientPackage]);
    }
    // make sure the file goes through the next gulp plugin
    this.push(file);

    // tell the stream engine that we are done with this file
    cb();
  });
}

module.exports = c9Reload;