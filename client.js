// var port = XXXX; INCLUDED BY MODULE
var socket = io(location.protocol + '//' + location.hostname + ":" + port);

socket.on('connect', function () {
  console.log("%c Connected to gulp-reload!", 'color: #0088dd');
  socket.on('refreshAll', function (msg) {
    location.reload();
  });
  socket.on('refreshStyles', function (msg) {
    refreshCss();
  });
});

// CREDIT: https://www.paulirish.com/2008/how-to-iterate-quickly-when-debugging-css/
function refreshCss() {
    var h, a, f;
    a = document.getElementsByTagName('link');
    for (h = 0; h < a.length; h++) {
        f = a[h];
        if (f.rel.toLowerCase().match(/stylesheet/) && f.href) {
            var g = f.href.replace(/(&|%5C?)forceReload=\d+/, '');
            f.href = g + (g.match(/\?/) ? '&' : '?') + 'forceReload=' + (new Date().valueOf())
        }
    }
}